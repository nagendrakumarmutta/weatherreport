import React from "react";

const WeatherTable=(props)=>{
    const {cityData} = props;
    const {main, dt} = cityData;
    const {temp_min, temp_max, pressure, humidity} = main;
    const date = new Date(dt);
    const day = date.getDate()+" / "+date.getMonth()+1+" / "+date.getFullYear();

  return(
    <div className="mt-7 flex  flex-col items-center md:flex  md:flex-col md:items-center"> 
        <table className="border-2 border-black w-60 md:w-60  h-9 text-center table lg:w-80 lg:h-12">
            <thead>
                <tr className="border-2 border-black  h-9 bg-orange-600 table-row w-full">
                    <th colSpan="4">Date:  {day}  </th>
                </tr>
                <tr className="border-2 border-black  h-9 text-center table-row bg-slate-400" >
                    <th colSpan="4">Temperature</th>
                </tr>
                <tr className="border-2 border-black  h-9 bg-slate-400">
                    <th className="border-2 border-black  h-9" colSpan="0">Min</th>
                    <th className="border-2 border-black  h-9" colSpan="4">Max</th>
                </tr>
            </thead>
            <tbody>
                <tr className="border-2 border-black h-9 bg-slate-400">
                    <td className="border-2 border-black h-9">{temp_min}</td>
                    <td className="border-2 border-black h-9">{temp_max}</td>
                </tr>
                <tr className="border-2 border-black h-9">
                    <td className="border-2 border-black h-9 font-medium">Pressure</td>
                    <td className="border-2 border-black h-9 font-medium">{pressure}</td>
                </tr>
                <tr className="border-2 border-black h-9">
                    <td className="border-2 border-black h-9 font-medium">Humidity</td>
                    <td className="border-2 border-black h-9 font-medium">{humidity}</td>
                </tr>
            </tbody>
        </table>
     
     </div>
    
  )
}

export default WeatherTable