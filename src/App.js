import WeatherPage from './WeatherPage';
import './input.css';

function App() {
  return (
    <div className="p-5">
      <WeatherPage />
    </div>
  );
}

export default App;
