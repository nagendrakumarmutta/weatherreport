import React, { useState } from "react"
import { useEffect } from "react"
import {AiFillQuestionCircle} from "react-icons/ai"
import WeatherTable from "./WeatherTable"

const WeatherPage =()=>{
    const [weatherData, setWeatherData] = useState([])
    const [city, setCity] = useState("")
    const [searchCity, setSearchCity] = useState("visakhapatnam");
    console.log(searchCity)

    const handleBtn=()=>{
        setSearchCity(city)
        setCity("")
    }

    useEffect(()=>{
        async function fetchApi(){
           const response = await fetch("https://api.openweathermap.org/data/2.5/forecast?appid=1635890035cbba097fd5c26c8ea672a1&q="+searchCity);
           const data = await response.json();
           const {list} = data;
           setWeatherData(list.slice(0,5))
        }

        fetchApi()
        setCity("")
    }, [searchCity])

    return(
        <>
        <div className="text-center">
           <div className="flex flex-col justify-center items-center gap-5 md:flex md:flex-col md:justify-center md:items-center md:gap-5 lg:flex lg:flex-row lg:justify-start ">
                <h1 className="text-orange-500 text-4xl font-['Roboto'] font-medium lg:text-5xl">Weather in your city</h1>
                <input value={city} className="outline-none border-2 border-orange-400 h-10 rounded-md lg:ml-20" placeholder="Search City" onChange={(e)=>{setCity(e.target.value)}}/>
                <div>
                <button onClick={handleBtn} className="p-1 rounded-md flex items-center gap-1 text-xl font-['Roboto'] bg-orange-500 text-white w-28 h-11 justify-center font-semibold"><AiFillQuestionCircle  className=""/>Search</button>
                </div>
           </div>
        </div>
        <div className="lg:flex lg:flex-row lg:gap-10 md:flex md:flex-col">
        {weatherData?.map(eachItem => <WeatherTable cityData={eachItem} />)}
        </div>
        </>
    )
}

export default WeatherPage